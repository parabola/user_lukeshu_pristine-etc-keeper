# Copyright 2016-2017 Luke Shumaker
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the COPYING file for more details.

PACKAGE = pristine-etc-keeper
VERSION = 20170131

topsrcdir = .
topoutdir = .
include $(topsrcdir)/build-aux/Makefile.head.mk

# The core of pristine-etc-keeper
files.sys.all += /etc/etckeeper/pristine/drain
files.sys.all += /etc/etckeeper/pristine/fill
files.sys.all += /var/lib/pristine-etc/chroot.lock
files.sys.all += /var/lib/pristine-etc/spool.lock

$(DESTDIR)/etc/etckeeper/pristine/%: $(srcdir)/%
	install -Dm755 $< $@
$(DESTDIR)/var/lib/pristine-etc/%.lock:
	mkdir -p $(@D)
	touch $@

# Convenience symlinks in bindir
files.sys.all += /usr/bin/pristine-etc-keeper

$(DESTDIR)/usr/bin/pristine-etc-keeper: $(DESTDIR)/etc/etckeeper/pristine/fill
	mkdir -p $(@D)
	ln -srfT $< $@

# pacman integration
files.sys.all += /usr/share/libalpm/hooks/zz-pristine-etc-keeper-post-install.hook

$(DESTDIR)/usr/share/libalpm/hooks/%.hook: %.hook
	install -Dm644 $< $@

# systemd integration
files.sys.all += /usr/lib/systemd/system/pristine-etc-keeper.service

$(DESTDIR)/usr/lib/systemd/system/%: $(srcdir)/%
	install -Dm644 $< $@

# Documentation
files.sys.all += /usr/share/doc/pristine-etc-keeper/README.md

$(DESTDIR)/usr/share/doc/pristine-etc-keeper/%: %
	install -Dm644 $< $@

include $(topsrcdir)/build-aux/Makefile.tail.mk
